import os
import unittest
import numpy
import pandas
import iris
from iris import iris_species, iris_cal, iris_remove

os.environ["All_PATH"] = 'iris.data'


class Iris_Test(unittest.TestCase):

    def test_loaded_data(self):
        """Test case used to verification if data is 
        correctly loaded  as not empty DataFrame object"""
        loaded_data = iris.load_file()
        self.assertEqual(type(loaded_data), pandas.DataFrame)
        self.assertGreater(len(loaded_data), 0)

    def test_species(self):
        """Test case used to get the number of species 
        and the number of Cases."""
        data = iris.load_file()

        # Return tuple contains cases and species and get
        # the numbers of it
        species = iris_species(data)
        species_number = len(species[1])
        cases_number = len(species[0])

        # Check if there is 3 species and 150 cases.
        self.assertEqual(species_number, 3)
        self.assertEqual(cases_number, 150)

    def test_z_post_processing(self):
        """Test case used to verification if data is
        correctly calculated by iris_cal() method from core module"""

        setosa_idx = 0
        versicolor_idx = 0
        virginica_idx = 0
        setosa_data = []
        versicolor_data = []
        virginica_data = []

        # Open file and calculate data
        with open(os.getenv('ALL_PATH'), "r") as my_file:
            names = ['sepal.length', 'sepal.width', 'petal.length', 'petal.width', 'species']
            data = my_file.readlines()
            setosa_data = [None] * len(data)
            versicolor_data = [None] * len(data)
            virginica_data = [None] * len(data)

            # Remove last line if there is not data
            if len(data[-1]) < 5:
                data.pop()

            # Divide data into species array
            for line in data:
                line_array = line.split(',')
                species = line_array[-1]
                if 'setosa' in species:
                    setosa_data[setosa_idx] = line_array[0:-1]
                    setosa_idx = setosa_idx + 1
                if 'versicolor' in species:
                    versicolor_data[versicolor_idx] = line_array[0:-1]
                    versicolor_idx = versicolor_idx + 1
                if 'virginica' in species:
                    virginica_data[virginica_idx] = line_array[0:-1]
                    virginica_idx = virginica_idx + 1

            # Remove empty rows from arrays
            setosa_data = [x for x in setosa_data if x is not None]
            versicolor_data = [x for x in setosa_data if x is not None]
            virginica_data = [x for x in setosa_data if x is not None]

            # sepal.length
            setosa_sl_max = max([x[0] for x in setosa_data])
            versicolor_sl_max = max([x[0] for x in versicolor_data])
            virginica_sl_max = max([x[0] for x in virginica_data])

            setosa_sl_median = sum([float(x[0]) for x in setosa_data]) / len(setosa_data)
            versicolor_sl_median = sum([float(x[0]) for x in versicolor_data]) / len(versicolor_data)
            virginica_sl_median = sum([float(x[0]) for x in virginica_data]) / len(virginica_data)

            setosa_sl_mean = numpy.mean([float(x[0]) for x in setosa_data])
            versicolor_sl_mean = numpy.mean([float(x[0]) for x in versicolor_data])
            virginica_sl_mean = numpy.mean([float(x[0]) for x in virginica_data])

            setosa_sl_min = min([x[0] for x in setosa_data])
            versicolor_sl_min = min([x[0] for x in versicolor_data])
            virginica_sl_min = min([x[0] for x in virginica_data])

            # sepal.width
            setosa_sw_max = max([x[1] for x in setosa_data])
            versicolor_sw_max = max([x[1] for x in versicolor_data])
            virginica_sw_max = max([x[1] for x in virginica_data])

            setosa_sw_median = sum([float(x[1]) for x in setosa_data]) / len(setosa_data)
            versicolor_sw_median = sum([float(x[1]) for x in versicolor_data]) / len(versicolor_data)
            virginica_sw_median = sum([float(x[1]) for x in virginica_data]) / len(virginica_data)

            setosa_sw_mean = numpy.mean([float(x[1]) for x in setosa_data])
            versicolor_sw_mean = numpy.mean([float(x[1]) for x in versicolor_data])
            virginica_sw_mean = numpy.mean([float(x[1]) for x in virginica_data])

            setosa_sw_min = min([x[1] for x in setosa_data])
            versicolor_sw_min = min([x[1] for x in versicolor_data])
            virginica_sw_min = min([x[1] for x in virginica_data])

            # petal.length
            setosa_pl_max = max([x[2] for x in setosa_data])
            versicolor_pl_max = max([x[2] for x in versicolor_data])
            virginica_pl_max = max([x[2] for x in virginica_data])

            setosa_pl_median = sum([float(x[2]) for x in setosa_data]) / len(setosa_data)
            versicolor_pl_median = sum([float(x[2]) for x in versicolor_data]) / len(versicolor_data)
            virginica_pl_median = sum([float(x[2]) for x in virginica_data]) / len(virginica_data)

            setosa_pl_mean = numpy.mean([float(x[2]) for x in setosa_data])
            versicolor_pl_mean = numpy.mean([float(x[2]) for x in versicolor_data])
            virginica_pl_mean = numpy.mean([float(x[2]) for x in virginica_data])

            setosa_pl_min = min([x[2] for x in setosa_data])
            versicolor_pl_min = min([x[2] for x in versicolor_data])
            virginica_pl_min = min([x[2] for x in virginica_data])

            # petal.width
            setosa_pw_max = max([x[3] for x in setosa_data])
            versicolor_pw_max = max([x[3] for x in versicolor_data])
            virginica_pw_max = max([x[3] for x in virginica_data])

            setosa_pw_median = sum([float(x[3]) for x in setosa_data]) / len(setosa_data)
            versicolor_pw_median = sum([float(x[3]) for x in versicolor_data]) / len(versicolor_data)
            virginica_pw_median = sum([float(x[3]) for x in virginica_data]) / len(virginica_data)

            setosa_pw_mean = numpy.mean([float(x[3]) for x in setosa_data])
            versicolor_pw_mean = numpy.mean([float(x[3]) for x in versicolor_data])
            virginica_pw_mean = numpy.mean([float(x[3]) for x in virginica_data])

            setosa_pw_min = min([x[3] for x in setosa_data])
            versicolor_pw_min = min([x[3] for x in versicolor_data])
            virginica_pw_min = min([x[3] for x in virginica_data])

        # Get data as a result of function needed to verify
        data = iris.load_file()
        calculations = iris_cal(data)
        calculations_basis = calculations[0]
        results = calculations[1]


if __name__ == '__main__':
    unittest.main()
