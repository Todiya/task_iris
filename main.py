import iris
from iris import iris_species, iris_cal, iris_remove, iris_manipulate


def run():
    data = iris.load_file()
    iris_species(data)
    iris_cal(data)
    iris_remove(data)
    iris_manipulate(data)
    iris.plot_data(data)


if __name__ == '__main__':
    run()
