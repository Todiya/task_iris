import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D


# Set working directory and load data
# set col names to the dataframe
def load_file():
    if os.getenv('ALL_PATH'):
        with open(os.getenv('ALL_PATH'), "r") as my_file:
            names = ['sepal.length', 'sepal.width', 'petal.length', 'petal.width', 'species']
            data = pd.read_csv(my_file, delimiter=',', names=names, engine='python')
            print(data.head(20))
            print(data.tail(20))
            print(data.describe())
            print(data.info())
            print(data.sum())
            print(data.cumsum())
            print(data.cummax())
            print(data.groupby('species').size())
            return data


def iris_species(data):
    species_list = list(data['species'].unique())
    print("Types of species: %s\n" % species_list)
    print("Dataset length: %i\n" % len(data))

    print("Sepal length range: [%s, %s]" % (min(data["sepal.length"]), max(data["sepal.length"])))
    print("Sepal width range:  [%s, %s]" % (min(data["sepal.width"]), max(data["sepal.length"])))
    print("Petal length range: [%s, %s]" % (min(data["petal.length"]), max(data["petal.length"])))
    print("Petal width range:  [%s, %s]\n" % (min(data["petal.width"]), max(data["petal.width"])))

    print("Sepal length variance:\t %f" % np.var(data["sepal.length"]))
    print("Sepal width variance: \t %f" % np.var(data["sepal.width"]))
    print("Petal length variance:\t %f" % np.var(data["petal.length"]))
    print("Petal width variance: \t %f\n" % np.var(data["petal.width"]))

    print("Sepal length mean:\t %f" % np.mean(data["sepal.length"]))
    print("Sepal width mean: \t %f" % np.mean(data["sepal.width"]))
    print("Petal length mean:\t %f" % np.mean(data["petal.length"]))
    print("Petal width mean: \t %f\n" % np.mean(data["petal.width"]))

    print("Sepal length median:\t %f" % np.median(data["sepal.length"]))
    print("Sepal width median: \t %f" % np.median(data["sepal.width"]))
    print("Petal length median:\t %f" % np.median(data["petal.length"]))
    print("Petal width median: \t %f\n" % np.median(data["petal.width"]))

    print("Sepal length stddev:\t %f" % np.std(data["sepal.length"]))
    print("Sepal width stddev: \t %f" % np.std(data["sepal.width"]))
    print("Petal length stddev:\t %f" % np.std(data["petal.length"]))
    print("Petal width stddev: \t %f\n" % np.std(data["petal.width"]))
    return data, species_list


def iris_cal(data):
    result = pd.pivot_table(data, index=['species'], aggfunc={min, max, np.mean, np.median}, fill_value=0)
    print(result)
    return data, result


def iris_remove(data):
    df_one = data.copy()
    df_new = data.copy()
    df_new = df_new.loc[df_new['sepal.length'] != df_new['sepal.length'].max()]
    df_new = df_new.loc[df_new['sepal.width'] != df_new['sepal.width'].max()]
    df_new = df_new.loc[df_new['petal.length'] != df_new['petal.length'].max()]
    df_new = df_new.loc[df_new['petal.width'] != df_new['petal.width'].max()]
    df_one.drop(df_new.index, axis=0, inplace=True)
    print(df_one, '\n')
    print(df_new)
    return data, df_new


def iris_manipulate(data):
    df_one = data.copy()
    df_one =data.copy()
    df_one = df_one.replace(df_one, 1)
    df_med = df_one.replace(0, df_one.median())
    print(df_one)
    return data


def plot_data(data):
    # scatter plot for correlation between petal width,petal length and sepal length
    # a high positive correlation between PetalWidth and PetalLength (0.96)
    # a high positive correlation between PetalLength and SepalLength (0.87)
    # a high positive correlation between PetalWidth and SepalLength (0.81)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    X = [data['petal.width'], data['petal.length']]
    n = 100
    ax.scatter(data["petal.width"], data["petal.length"], data["sepal.length"])
    ax.set_xlabel('petal.width')
    ax.set_ylabel('petal.length')
    ax.set_zlabel('sepal.length')
    data.plot(kind='scatter', x='petal.width', y='petal.length')
    data.plot(kind='scatter', x='petal.length', y='sepal.length')
    data.plot(kind='scatter', x='petal.width', y='sepal.length')
    plt.tight_layout(pad=0.5)
    plt.show()

    # boxplot for species
    data.plot(kind='box', subplots=True, layout=(2, 2), sharex=False, sharey=False)
    plt.show()
    plt.figure(figsize=(12, 10))
    plt.subplot(2, 2, 1)
    sns.violinplot(x='species', y='sepal.length', data=data)
    plt.subplot(2, 2, 2)
    sns.violinplot(x='species', y='sepal.width', data=data)
    plt.subplot(2, 2, 3)
    sns.violinplot(x='species', y='petal.length', data=data)
    plt.subplot(2, 2, 4)
    sns.violinplot(x='species', y='petal.width', data=data)
